"use strict";

(function(){
  function httpInterceptor() {
    let _httpInterceptor_ = {};
    
    _httpInterceptor_.request = config => {
      config.headers.Authorization = 'Bearer 800813555';
      console.log(config);
      return config;
    }

    _httpInterceptor_.requestError = rejection => {
      console.log(rejection);
      return rejection;
    }

    _httpInterceptor_.response = response => {
      console.log(response);
      return response;
    }

    _httpInterceptor_.responseError = rejection => {
      console.log(rejection);
      return rejection;
    }

    return _httpInterceptor_;
    
  }

  httpInterceptor.$inject = [];

  angular
    .module('hotelApp')
      .factory('httpInterceptor', httpInterceptor);

})();