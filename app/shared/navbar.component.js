"use strict";

(function(){

  function ctrlFn() {
  }

  angular
    .module('hotelApp')
      .component('navbar', {
        bindings: {
          userName: '@'
        },
        transclude: true,
        controller: ctrlFn,
        template: ($element, $attrs) => `
          <md-toolbar>
            <div class="md-toolbar-tools">
              <ng-transclude></ng-transclude>
              <span flex></span>
              <h2>
                <span>HotelApp</span>
              </h2>
              <span flex></span>
              <md-button class="md-raised" aria-label="Hooola {{$ctrl.userName}}">
                {{$ctrl.userName}}
              </md-button>
            </div>
          </md-toolbar>
        `
       });
})();